##Bienvenue sur Roquefort !

Ce programme écrit en Ruby est en réalité une suite de scripts destinés à interagir avec le LDAP Aurore.
Celui ci n'a pas vocation à remplacer Brie, cela prendrait trop de temps ou pire encore serait intile. 
Non l'objectif est de compléter ce que Brie ne fait pas.

Pourquoi ne pas coder sur Brie me dîtes vous ? Parce que je ne connais pas trop le python et que je me suis un peu perdu dans le code quand j'ai commencé à regarder Brie, c'est aussi simple que ça.

##Les objectifs

- Gestions des entrées DNS
- Gestion de l'aspect POSIX du LDAP (Groupes,Membres,...)
- Permettre l'utilisation d'un shell optimisé pour intéragir avec le LDAP
- Une future transplantation sur le fameux framework Ruby on Rails qui permettrait une utilisation par le web
- ... d'autres choses au fur et à mesure que les idées me viennent !

## Comment l'installer?

La première chose nécessaire est d'installer Ruby, malheureusement celui ci n'est pas à jour dans la majorité des repositories il
faudra donc le compiler !

Le site web officiel fera l'affaire, jetez y un coup d'oeil: [Ruby Lang](https://www.ruby-lang.org/fr/)

Une fois qu'il est installé est compilé ce n'est pas fini car Roquefort utilise des gems pour communiquer avec le serveur LDAP par
exemple. Une gem est une extension de Ruby créé par la communautées, celles ci sont disponibles sur le site de RubyGems.

Commencez donc par cloner ce repository git, une fois que c'est fait lancez rake dans le repertoire principal et celui-ci devrait installer
les gems tout seul grâce au Rakefile !

## Et ensuite?

J'ai un peu la flemme de faire un wiki mais en gros une fois le projet cloné on va dans le repertoire principale puis on execute rake qui installera les
dépendances et on execute irb. Attention l'uid linux doit être le même que l'uid ldap.

Ensuite on entre son mot de passe et voilà on peut executer quelques fonctions,

dns(from,to,type,path) cree une nouvelle entrée dns, type = "A" ou "CNAME" et path est le repertoire ou= dans lequel est enregistré l'entrée
user_summary(uid) résumé d'un membre
uid_allocate(all) alloue un uid number (compris entre UID_BEGIN et UID_MAX) à tout les membres

Y a des fonctions pour les password mais comme on a pas accès à un serveur smtp depuis le réseau on peut pas les envoyer aux membres

ssh! do |ssh|
ssh.exec!("hostname")
end

Execute hostname sur la machine principale (voir default/ruby-ldap.config.rb)

ping(host) renvoie true sur l'host ping

get_logs(loglocation,greppattern,needsudo(booleen)) envoie un grep du log entré dans loglocation sur la machine principale

voila voila grossomodo, voir le fichier de config dans /default pour des modifs !
