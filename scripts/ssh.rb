#Dans la mesure ou Roquefort ne tourne pas forcement sur la machine qui accueille les logs de l'associations ce script permettra de lancer des commandes SSH 
#Et d'en recuperer le resultat. Utile lorsque Roquefort sera transplante sur Rails

require 'net/ssh'


##
# Wrapper pour utiliser Net::SSH.start plus rapidement avec les infos du LDAP


def ssh!

	return false unless block_given?

	Net::SSH.start(SSH_HOST,get_uid_by_dn(LDAP.bind_dn),:password => LDAP.bind_passwd) do |ssh|

		yield ssh
	end
end

def get_log(log_location,grep_pattern,need_sudo)

	ssh! do |logServer|

		logServer.exec!("#{'sudo ' if need_sudo}cat #{log_location}|grep -i #{grep_pattern}") do |channel,stream,data|

			return data if stream == :stdout
		end
	end
end

	
