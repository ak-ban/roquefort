#Gère l'interaction des utilisateurs avec le réseau

def get_machines_by_dn(dn)

	machines_dn,machines_data = [],[]

	machine_search = LDAP.search(:base=>"cn=machines,"+dn,:filter=>WILDCARD)
	machine_search.each { |m| machines_dn << m.dn if m.dn.split(",")[1] == "cn=machines" } unless machine_search.nil?
	
	return false if machines_dn.empty?
	
	machines_dn.each { |dn|

		hash = {}
		machine_info = LDAP.search(:base=>dn,:filter=>WILDCARD)
			
		hash[:dns_name] = machine_info[2].dlzhostname.first
		hash[:mac] = machine_info[1].dhcphwaddress.first.gsub("ethernet ","")
		hash[:ip] = machine_info[2].dlzdata.first
		machines_data << hash
			}

	return machines_data
	
end

def get_dhcp_logs_by_dn(dn)

	machines = get_machines_by_dn(dn)
	array = []
	
	machines.each { |m|
		hash = {}
		hash[:name] = m[:dns_name]
		hash[:log] = get_log(SYSLOG,m[:ip],true) || false
			array << hash	
			} 
	return array
end

	
