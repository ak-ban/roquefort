#Cotisations Aurore

class Net::LDAP::Cotisation < Net::LDAP::Entry

	def initialize(hash)

		@id = hash[:id]
		@actionuser = hash[:actionuser]
		@actionuserinfo = hash[:actionuserinfo]
		@amount = hash[:amount]
		@valid_month = hash[:valid_month]
		@cashed = hash[:cashed]
		@cashed = false if @cashed.empty?
	end

	attr_reader :id,:actionuser,:actionuserinfo,:amount,:valid_month,:cashed

	def isCashed?
		!self.cashed.is_a? FalseClass
	end
end

def get_cotisations_by_dn(dn)

	filter = Net::LDAP::Filter.eq "objectclass","auroreCotisation"
	search = LDAP.search(:base=>dn,:filter=>filter)

	array = []

	search.each { |cotisation|

		hash = {}

		hash[:id] = cotisation.cn.first
		hash[:actionuser] = cotisation["x-action-user"].first
		hash[:actionuserinfo] = cotisation["x-action-user-info"].first
		hash[:amount] = cotisation["x-amountpaid"].first
		hash[:valid_month] = cotisation["x-validmonth"]
		hash[:cashed] = cotisation["x-paymentcashed"]

		array << Net::LDAP::Cotisation.new(hash) 
			}
	array
end

def get_all_cotisations

	filter = Net::LDAP::Filter.eq "objectclass","auroreCotisation"
	search = LDAP.search(:filter=>filter)
	
	array = []

	search.each { |cotisation|

		hash = {}

		hash[:id] = cotisation.cn.first
		hash[:actionuser] = cotisation["x-action-user"].first
		hash[:actionuserinfo] = cotisation["x-action-user-info"].first
		hash[:amount] = cotisation["x-amountpaid"].first
		hash[:valid_month] = cotisation["x-valid-month"]
		hash[:cashed] = cotisation["x-paymentcashed"] || false

		array << Net::LDAP::Cotisation.new(hash) 
			}
	array
end

def user_valid_month_by_dn(dn)
	months=[]
	get_cotisations_by_dn(dn).each{ |c| months += c.valid_month }
	months.uniq
end

def sum_all
	ret=0
	get_all_cotisations.each { |c| ret+=c.amount.to_i }
	ret
end

def sum_cashed
	ret=0
	get_all_cotisations.each { |c| ret+=c.amount.to_i if c.isCashed? }
	ret
end


