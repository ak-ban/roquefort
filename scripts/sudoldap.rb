#!/usr/bin/env ruby

require 'net/ldap'
require 'rubygems'

if `whoami`.chomp != "root"
	puts "Seul le superutilisateur peut ajouter un membre"
elsif ARGV.size==0
	puts "Vous devez preciser un utilisateur a ajouter"
else

ldap = Net::LDAP.new
	ldap.host="ldap"

USER_USING_SUDO = `logname`.chomp

BASENAME="rub"

filter_admin_dn = Net::LDAP::Filter.eq("uid",USER_USING_SUDO)

search_for_dn = ldap.search(:base=>"dc=#{BASENAME},dc=u-psud,dc=fr",:filter=>filter_admin_dn)


ADMINDN=search_for_dn[0][:dn][0] #Recupere le DN depuis ldapsearch

puts "Mot de passe(#{USER_USING_SUDO}):"

	system "stty -echo"
	ADMINPW= $stdin.gets.chomp;puts"\n"


userUID=ARGV[0]
SUDODN="cn=sudoldap,ou=access,ou=groupes,dc=rub,dc=u-psud,dc=fr"
SUDONAME="sudoldap"
LDAP_HOST = "ldap"
delete=false
filter=Net::LDAP::Filter.eq("uid",userUID)

ARGV.each{ |arg| 
	if arg=="-d"
		delete=true
		userUID=ARGV[1]
	end
} 

ldap.auth ADMINDN,ADMINPW

unless ldap.bind 
	
		puts "Echec de l'authentification au serveur LDAP" #Bind sur le LDAP, si echec n'execute pas le reste
	
	else 
		if delete
			
		ldap.modify :dn => SUDODN, :operations => [[:delete,:memberUid,userUID]]
		
		else

		if ldap.search(:base=>"dc=#{BASENAME},dc=u-psud,dc=fr", :filter => filter).size > 0
				
				ldap.add_attribute SUDODN,:memberUid,userUID 
			

				else 
				puts "Cet utilisateur n'existe pas"
				nowhere=true
			end	
		end	
	end 
puts ldap.get_operation_result[:message] unless nowhere
end




