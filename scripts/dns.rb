require 'net/ldap'

##
# The Roquefort DNS class

class Net::LDAP::DNS < Net::LDAP::Entry

	ZONEDN = "dlzZoneName="+ZONE_NAME+",ou=global,ou=dns,"+BASE

	
	def initialize(dn=nil)
		@change=false
		@dn = dn
		@myhash={}
		@myhash[:global_attributes] = {
		
			:objectClass => ["dlzAbstractRecord","top"],
			:dlzrecordID => "1",
			:dlzTTL => 3600.to_s,
			}
		@type=""
	end

	attr_accessor :dn,:change,:type

	##
	# Return the record type.
	# Also aliased as :record

	def record_type
		@myhash[:data_attributes][:dlztype]
	end
	alias_method :record,:record_type

	##
	# Change the content of the entry, if the last argument change is set to true then it won't affect the @change instance variable.
	# Also aliased as :set
	
	def record_processing(from,to,type,change=false)
		
		case type
			
			when "A"
				@myhash[:data_attributes] =
				{
					:dlzHostName => from,
					:dlzIPAddr => to,
					:dlzType => "A"
				}
				@myhash[:global_attributes][:objectClass].push "dlzARecord"
				self.type = "A"

			when "AAAA"
				@myhash[:data_attributes] =
				{
					:dlzHostName => from,
					:dlzIPAddr => to,
					:dlzType => "AAAA"
				}
				self.type = "AAAA"
			when "CNAME"
				@myhash[:data_attributes] =
				{
					:dlzHostName => from,
					:dlzData => to,
					:dlzType => "CNAME"
				}
				@myhash[:global_attributes][:objectClass].push "dlzGenericRecord"
				self.type = "CNAME"
			else

				raise Net::LDAP::LdapError, "Ce type n'est pas compatible avec Roquefort"
	      		end
			self.change = true if !change
	end
	alias_method :set,:record_processing


	##
	# Return the sub-directory the entry should be saved into using his dn, if dn is nil then return "roquefort"	

	def where?
		return "roquefort" if self.dn.nil?

		old_notation = /dlzrecordid=[0-9]/

		split_dn = self.dn.split(",")

		if (split_dn[0] =~ old_notation) == 0

			return split_dn[2].gsub("ou=","")

		else 
			return split_dn[1].gsub("ou=","")
		end
	end
		
	##
	# Delete the entry from the LDAP using his dn
	
	def delete
		LDAP.delete :dn => self.dn 		
	end

	##
	# Save the DNS entry into the LDAP and return true. Otherwise it'll return false while printing the LDAP returned error
	

	def save(path=self.where?)

		if !self.change ; puts "Entrée DNS non enregistrée car aucun changements réalisés" ; return false ; end
		
		if self.dn.nil? #S'il s'agit d'une nouvelle entrée
			
			path.insert(0,"ou=")
			create_ou_entry(path+","+ZONEDN) unless dn_exist?(path+","+ZONEDN) 

			self.dn = "dlzhostname="+@myhash[:data_attributes][:dlzHostName]+","+path+","+ZONEDN
			attr = @myhash[:global_attributes].merge(@myhash[:data_attributes])
			LDAP.add(:dn => self.dn, :attributes => attr) unless dn_exist? self.dn
			$attr = attr
			$dn = dn

			if LDAP.get_operation_result[:code]!=0
				raise Net::LDAP::LdapError, LDAP.get_operation_result[:message]
				return false
			end
		else
			self.delete #On supprime le dn actuel du LDAP (obligation avec le LDAP Aurore)
			
				tmp = Net::LDAP::DNS.new #On cree une nouvelle instance
				case self.type
					when "CNAME"
						info = [@myhash[:data_attributes][:dlzHostName],@myhash[:data_attributes][:dlzData],"CNAME"]
					else
						info = [@myhash[:data_attributes][:dlzHostName],@myhash[:data_attributes][:dlzIPAddr],"A"]
				end
			tmp.set info.first,info[1],info[2]
			tmp.save(path) #Enregistrement
		end
		true
	end

		
				 
end

def dns(from,to,type,path)

	dns = Net::LDAP::DNS.new
	dns.set(from,to,type)
	dns.save(path)
end

##
# Get DNS entries from LDAP and return an array of the following shape [ [host,type,data],...] 
def get_dns_entries

	dns_records = LDAP.search(:filter=>GLOBAL_DNS)
	dns_array = []
	dns_records.each{ |dns| dns_array << [dns.dlzhostname.first,dns.dlztype.first,(dns["dlzipaddr"].first||dns["dlzdata"].first)] }

	return dns_array
end

def get_dns(name)
	filter = GLOBAL_DNS & Net::LDAP::Filter.eq("dlzhostname",name+"*")
	search = LDAP.search(:filter=>filter)

	if search.size > 1
		puts "Requete ambigue, preciser: \n"
			search.each_with_index{ |entry,id| puts "[#{id}] "+entry.dlzhostname.first }
			index = gets.chomp.to_i
	elsif search.size==1
		index = 0
	else return false
	end

	search[index].to_dns
end


require 'net/ping/external' 

def ping(host)
	ping_instance = Net::Ping::External.new(host)
	ping_instance.ping #Renvoie true si l'hote ping et false sinon
end
