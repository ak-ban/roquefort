#Ce script gère l'aspect posix du ldap

##
# Returns an array including every uid number already allocated to a member

def allocated_uid_simple(ldap)

        filter = Net::LDAP::Filter.ne("uidNumber","-1")
        filter = Net::LDAP::Filter.join(filter,MEMBERS)

        array_uid = Array.new
        members_posix = ldap.search(:filter=>filter)

        members_posix.each{|member| array_uid << member[:uidNumber][0].to_i}

        return array_uid

end

##
# Returns an array including every uid number already allocated to a member with the member's dn 

def allocated_uid(ldap)

        allocated = allocated_uid_simple(ldap)

        allocated_with_uid = Array.new
        allocated.each{|uid| allocated_with_uid << [uid,uid_by_uidnumber(ldap,uid)]}

        return allocated_with_uid
end

##
# Returns the uid of a member having the uid number uidnumber

def uid_by_uidnumber(ldap,uidnumber)

        filter = Net::LDAP::Filter.eq("uidNumber",uidnumber.inspect)

        
        result = ldap.search(:filter=>filter)

        if result.size==0
                puts "Aucun utilisateur correspondant"
                return false
        else
                return result[0][:uid][0]
        end
end

##
# Returns an array including every remaining uid number

def remaining_uid(ldap)

possible_uid = BEGINUID.upto(MAXUID).to_a

return possible_uid - allocated_uid_simple(ldap) 

end

##
# For each member that has no uid number it will give one

def uid_allocate_all(ldap)
        
	raise Net::LDAP::LdapError, "Vous êtes anonyme alors que la fonction nécessite les droits d'écriture" if ldap.anonymous?        


        uid_list = remaining_uid(ldap)
        fail_list = Array.new

        members = ldap.search(:filter=>MEMBERS)

        members.each{ |member|

                if member[:uidNumber][0] == "-1"
                        ldap.replace_attribute member.dn, :uidNumber, uid_list[0].inspect
                        uid_list.delete_at(0)
                        puts ldap.get_operation_result[:message]
                                if ldap.get_operation_result[:code] != 0 then fail_list.push member[:uid][0] end
                end
        }

       unless fail_list.empty?
        puts "L'opérationa  échoué pour,\n"
        p fail_list
        end
end

##
# Generate a password for every members having an uid number lesser than uidmax and send them by mail

def generate_password_all(ldap,uidmax=MAXUID) 

        	raise Net::LDAP::LdapError, "Vous êtes anonyme alors que la fonction nécessite les droits d'écriture" if ldap.anonymous?
       		filter = Net::LDAP::Filter.join(MEMBERS,NO_PASSWORD)
		filter = filter & Net::LDAP::Filter.le("uidNumber",uidmax.inspect)        

                members = ldap.search(:filter=>filter)

                members.each{ |member|
                        password=rand(36**PW_LENGTH).to_s(36) #Generation d'un mot de passe    
                        ldap.add_attribute member.dn, :userPassword, password.generate
                        password_mail(member[:mail][0],password,member[:cn][0],member[:uid][0])
                }
                puts ldap.get_operation_result[:message]
end

##
# Delete every password excluding netadmins

def delete_password_all(ldap) 

        raise Net::LDAP::LdapError, "Vous êtes anonyme alors que la fonction nécessite les droits d'écriture" if ldap.anonymous?
        
	filter_responsable = Net::LDAP::Filter.contains("memberOf",ADMIN_GROUP_DN)
        filter = Net::LDAP::Filter.negate(filter_responsable) & MEMBERS

        users = ldap.search(:filter=>filter)

        users.each{|member| ldap.delete_attribute member.dn,:userPassword}
end

##
# Reset every password excluding netadmins

def reset_password_all(ldap)
        delete_password_all(ldap)
        generate_password_all(ldap)
end

##
# Reset password for a specific uid

def reset_password_user(ldap,uid)

	raise Net::LDAP::LdapError, "Vous êtes anonyme alors que la fonction nécessite les droits d'écriture" if ldap.anonymous? 

        dn = get_dn_by_uid(uid)

        ldap.delete_attribute dn,:userPassword

        generate_password_all(ldap) #Comme il n'y a que l'utilisateur qui n'aura plus de password on peut utiliser cette fonction
end


       
