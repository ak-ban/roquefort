require 'net/smtp'


def getmails(ldap)

        filter_member = Net::LDAP::Filter.eq("objectClass","pacatnetMember")

        members = ldap.search(:base=>BASE,:filter=>filter_member)

        mails = []

        members.each{|entry| mails <<  entry[:mail][0]}

        mails.delete_if{|mail| mail==""}

        return mails

end

def write_mail_to_file(ldap,file)

        mails = getmails(ldap)

        string_tmp_mails = ""

        mails.each{|mail| string_tmp_mails <<  mail+"\n"}

        mailing_list = File.open(file,"w+")

        mailing_list.write(string_tmp_mails)

        mailing_list.close
end

def mail_password(mail,password,name,uid)

        message= <<MESSAGE_END

                From: #{ASSO_NAME} <#{ASSO_MAIL}>
                To: #{name} <#{mail}>
                Subject: Votre mot de passe #{ASSO_NAME}

        Bonjour,

        Un nouveau mot de passe #{ASSO_NAME} vient d'être créé pour vous. Celui ci vous permettra d'accèder à nos différents services, notez le bien !

        Login: #{uid}
        Mot de passe: #{password}

        
                #{ASSO_NAME},

MESSAGE_END

        Net::SMTP.start(SMTP_SERVER,SMTP_PORT,SMTP_DOMAIN,SMTP_ACCOUNT,SMTP_PASSWORD) do |smtp|
                smtp.send_message message, mail
        end
end


