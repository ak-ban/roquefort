#skeleton for ldap usage

load './default/ldap_class.rb'
load './default/ruby-ldap.conf.rb'
		


def anonymous_ldap(host=LDAP_DEFAULT_HOST,port=LDAP_DEFAULT_PORT) #Connexion au LDAP de manière anonyme

        ldap = Net::LDAP.new
               ldap.host=host
               ldap.port=port
               ldap.base=BASE
	       ldap.bind #Verification
	       
        return ldap
end

def connect_ldap(host=LDAP_DEFAULT_HOST,port=LDAP_DEFAULT_PORT,binddn="ask",bindpw="ask") #Connexion au LDAP avec identifiants puis bind

        #Connexion au serveur ldap

        ldap = Net::LDAP.new
               ldap.host=host
               ldap.port=port
               ldap.base=BASE


                if binddn == "ask" #Si DN non precise

                        user_linux = `logname`.chomp #On recupere le login de l'utilisateur

                        filter_dn = Net::LDAP::Filter.eq("uid",user_linux)         
                        search_for_dn = ldap.search(:base=>BASE,:filter=>filter_dn)
                        binddn = search_for_dn[0][:dn][0] #On recupere le DN       

                        puts "Mot de passe(#{user_linux}):"

	                system "stty -echo"
	                bindpw = $stdin.gets.chomp;puts"\n"
                        `stty echo`
                        puts ""
                end

        ldap.auth binddn,bindpw
        
        unless ldap.bind 

                puts "Echec de l'authentification LDAP"
                puts ldap.get_operation_result[:message]
         end        

	
        return ldap
end


#Connexion automatique

LDAP = connect_ldap if AUTOMATIC_CONNECTION

def filter_uid(uid) #Renvoie un filtre uid=
        Net::LDAP::Filter.eq("uid",uid)
end

def get_member_by_uid(uid,ldap=true)

        ldap=LDAP if ldap #Si connexion automatique utiliser l'objet LDAP
	
	filter = filter_uid(uid+"*")
        filter = Net::LDAP::Filter.join(filter,MEMBERS)
        uid_search = ldap.search(:filter=>filter)

        if uid_search.size > 1
                
                puts "Requête ambigüe, plus d'un DN correspondant\n"

                uid_search.each_with_index{ |entry,index|

                        puts "[#{index}]"+entry.dn
                }

                puts "Quel DN utiliser?\n"
                 
                dn_index = gets.chomp.to_i 
        else
        dn_index=0
        end

        return uid_search[dn_index]
end


def get_member_by_dn(dn)
	get_member_by_uid(get_uid_by_dn(dn))
end

def get_dn_by_uid(uid,ldap=true) #Renvoie le DN d'un utilisateur en se basant sur un uid ou une portion d'uid

        ldap=LDAP if ldap

	filter = filter_uid(uid+"*")
        filter = Net::LDAP::Filter.join(filter,MEMBERS)
        uid_search = ldap.search(:filter=>filter)

        if uid_search.size > 1
                
                puts "Requête ambigüe, plus d'un DN correspondant\n"

                uid_search.each_with_index{ |entry,index|

                        puts "[#{index}]"+entry.dn
                }

                puts "Quel DN utiliser?\n"
                 
                dn_index = gets.chomp.to_i 
        elsif uid_search.size == 1
        dn_index=0
        else
	return false 
	end

        return uid_search[dn_index][:dn][0]
end

def get_alldn_by_uid(uid,ldap=true) #Retourne un tableau de DN

	ldap = LDAP if ldap

        filter = filter_uid(uid+"*") & MEMBERS
        filtername = filter_uid("*."+uid+"*") & MEMBERS

        array_of_dn = Array.new

        uid_search = ldap.search(:filter=>filter)
        uid2_search = ldap.search(:filter=>filtername)
        
        uid_search.each{|entry| array_of_dn << entry.dn} #Ajout de tout les résultats au tablea
        uid2_search.each{|entry| array_of_dn << entry.dn}

        return array_of_dn.uniq #Supprime les doublons
end

def get_uid_by_dn(dn) #Renvoie l'uid d'un membre depuis son dn
        dn.split(",")[0].gsub(/(.)*=/,"")
end

def get_name_by_uid(uid,ldap=true)

	ldap=LDAP if ldap

        filter = filter_uid(uid)
        filter = Net::LDAP::Filter.join(filter,MEMBERS)
        ldap.search(:filter=>filter).first.cn
end


def get_name_by_dn(dn,ldap=true)

	ldap=LDAP if ldap
        get_name_by_uid(get_uid_by_dn(dn),ldap)
end

def bind_info(ldap=true)

	ldap=LDAP if ldap
	binddn = ldap.bind_dn
	return binddn if binddn=="Anonymous"	

	uid = get_uid_by_dn(binddn)
	cn = get_name_by_dn(binddn,ldap)

	{ :dn => binddn,:uid=> uid, :cn => cn}
end

def get_room_by_dn(dn,ldap=true)
	ldap=LDAP if ldap
	search = ldap.search(:base=>ROOMDN)
		search.each { |room| return room.cn.first if room["x-memberIn"].first == dn }
	nil
end

def get_room_by_uid(uid)
	get_room_by_dn(get_dn_by_uid(uid))
end

def user_summary(uid,detail=false)

	dn = get_dn_by_uid(uid)
	user = get_member_by_dn(dn)
	puts "Utilisateur: "+user.uid
	puts "Nom: "+user.cn.first
	puts "Chambre: "+(get_room_by_dn(dn) || "SDF")
	puts "uidNumber: "+user.uidnumber
	puts "Cotisations: \n"
		get_cotisations_by_dn(dn).each { |c|
			puts c.id+" --> "+c.amount+"e (fait par #{c.actionuserinfo})" }
	puts "Mois Valides: "
	if user_valid_month_by_dn(dn).empty?
	puts "nil"
	else
	print user_valid_month_by_dn(dn)
	end
	
	puts "\nMachines: \n"
	unless !get_machines_by_dn(dn)
	get_machines_by_dn(dn).each { |machine| 
		puts "\nNom: "+machine[:dns_name]
		puts "MAC: "+machine[:mac]
		puts "IP: "+machine[:ip]
		puts "Ping? "+ping(machine[:ip]).inspect if detail
				}
	else puts "nil"
	end	
end
	
def dn_exist?(dn,ldap=true)

	ldap=LDAP if ldap
	
	!ldap.search(:base=>dn).nil?
end

def create_ou_entry(dn,description=false,ldap=true)

	ldap=LDAP if ldap

	ou_attributes = {
		:objectclass => ["organizationalUnit","top"]
		}

	ou_attributes[:description] = description if description.is_a? String

	LDAP.add(:dn=>dn,:attributes=>ou_attributes)

	LDAP.get_operation_result
end
