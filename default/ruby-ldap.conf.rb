#Configuration de Roquefort

#Adresse du serveur LDAP

LDAP_DEFAULT_HOST="ldap"
LDAP_DEFAULT_PORT=389

#Adresse du serveur principal (SSH)

SSH_HOST="atlas.rub.u-psud.fr"

#Emplacement des logs dans le serveur principal

SYSLOG = "/var/log/syslog"

#Creation automatique d'un objet ldap 

AUTOMATIC_CONNECTION=true

#Base de recherche

BASE="dc=rub,dc=u-psud,dc=fr"

#Zone DNS

ZONE_NAME = "rub.u-psud.fr"

BEGINUID=10167
MAXUID=10999

#Filtres

MEMBERS = Net::LDAP::Filter.eq("objectClass","pacatnetMember")
HAS_PASSWORD = Net::LDAP::Filter.present?("userPassword")
NO_PASSWORD = Net::LDAP::Filter.negate(HAS_PASSWORD)
NO_UID_DEFINED = Net::LDAP::Filter.eq("uidNumber","-1")
UID_DEFINED = Net::LDAP::Filter.negate(NO_UID_DEFINED)
WILDCARD = Net::LDAP::Filter.present? "objectclass"
GLOBAL_DNS = Net::LDAP::Filter.eq("objectclass","dlzabstractrecord") & Net::LDAP::Filter.ne("objectclass","organizationalrole")


#Asso

ASSO_NAME = "Ciby-Net"
ASSO_MAIL = "cibynet@u-psud.fr"
SMTP_SERVER = "smtp.u-psud.fr"
SMTP_PASSWORD= "secret"
SMTP_PORT=25
SMTP_DOMAIN="u-psud.fr"
SMTP_ACCOUNT="cibynet"


#DN remarquables

ADMIN_GROUP_DN = "cn=responsablereseau,ou=groupes,"+BASE

#Base de chambres

ROOMDN="ou=chambres,"+BASE

#Securité write entry

NOWRITE=true
