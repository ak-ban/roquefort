#Modifications des classes Ruby par défaut afin d'avoir des classes appropriées à Roquefort

require 'net/ldap'
class Net::LDAP #Ajout d'une methode à la classe LDAP pour recuperer le DN bind
   def bind_dn
        @auth[:username] || "Anonymous"
   end

  def bind_passwd
	@auth[:password] || false
  end

   def anonymous?
	return true if @auth[:method] == :anonymous
	false
   end

end

class Net::LDAP::Password #Credits: https://github.com/ruby-ldap/ruby-net-ldap/blob/master/lib/net/ldap/password.rb
	class << self
    attribute_value = ""
    def generate(type, str)
       case type
         when :md5
            attribute_value = '{MD5}' + Base64.encode64(Digest::MD5.digest(str)).chomp!
         when :sha
            attribute_value = '{SHA}' + Base64.encode64(Digest::SHA1.digest(str)).chomp!
         when :ssha
	salt = SecureRandom.random_bytes(16)
            attribute_value = '{SSHA}' + Base64.encode64(Digest::SHA1.digest(str + salt) + salt).chomp!
         else
            raise Net::LDAP::LdapError, "Unsupported password-hash type (#{type})"
         end
      return attribute_value
    end
end
end

class Net::LDAP::Entry

	

        def initialize(dn = nil)
	 @myhash = {}
	 @myhash[:dn] = [dn]
	 @ops = []
	 @nowrite=NOWRITE
	end

	def nowrite
	@nowrite
	end

	def ops
	@ops
	end	
	
	def toggle
	@nowrite ? @nowrite=false : @nowrite=true
	end

	def summary
		@myhash.each{ |attribute,value| puts " #{attribute} -> #{value}" }  
	end
	def []=(name, value)
	
	if self[name].empty?
	@ops.push [:add,name.to_sym,value] unless self.nowrite
	else
	@ops.push [:replace, name.to_sym, value] unless self.nowrite
	end
	@myhash[self.class.attribute_name(name)] = Kernel::Array(value)
	
	end

	def mail
                @myhash[:mail].first
        end
        
        def groups
                @myhash[:memberof]
        end
        
        def uid
                @myhash[:uid].first
        end
        
        def uidnumber
                @myhash[:uidnumber].first
        end

        def isMember?(group)
                return false if @myhash[:memberof].nil?
                self.groups.include?("cn=#{group},ou=groupes,#{BASE}")
        end

        def is_Aidemembre?
                self.isMember?("admin")
        end

        def is_Admin?
                self.isMember?("responsablereseau")
        end
	
	def is_allowedSSH?
		self.isMember?("SSH")
	end

	def write_ldap
		unless defined?(LDAP).nil?
		LDAP.modify :dn=>self.dn, :operations => @ops
		@ops.clear
		puts LDAP.get_operation_result[:message]
		return false if LDAP.get_operation_result[:code] != 0
		else
		puts "Veuillez utiliser l'objet LDAP"
		return false
		end
		true
	end

	def to_dns
		dns = Net::LDAP::DNS.new(self.dn)
		
			case self.dlztype.first
				when "A"	
					dns.set(self.dlzhostname.first,self.dlzipaddr.first,"A",true)					
				when "CNAME"
					dns.set(self.dlzhostname.first,self.dlzdata.first,"CNAME",true)
				else raise Net::LDAP::LdapError, "Type d'enregistrement inconnu"
			end

		dns.type = self.dlztype.first
			return dns
	end

end

class String 
        def generate #Permet d'appliquer la méthode directement sur des chaines de caractères
                Net::LDAP::Password.generate(:ssha,self)
        end
end


